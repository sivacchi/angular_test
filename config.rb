# Require any additional compass plugins here.

# Set this to the root of your project when deployed:
http_path = "/"
css_dir = "css"
sass_dir = "_scss"
images_dir = "img"
javascripts_dir = "javascripts"
cache=false

# 出力形式を指定してください。コマンドオプションで上書きもできます。
# output_style = :expanded or :nested or :compact or :compressed
output_style = (environment == :production) ? :compressed : :expanded

# 相対パスを指定する
relative_assets = true

# 行番号コメントの挿入しない
#line_comments = false

# Sassのデバッグ情報を表示（IE8でCSSが正しく読めなくなるので気をつける）
#sass_options = {:debug_info => true}

# 画像にクエリをつけない
#asset_cache_buster = "none"

# キャッシュを保存しない
cache = false

# If you prefer the indented syntax, you might want to regenerate this
# project again passing --syntax sass, or you can uncomment this:
# preferred_syntax = :sass
# and then run:
# sass-convert -R --from scss --to sass . scss && rm -rf sass && mv scss sass
