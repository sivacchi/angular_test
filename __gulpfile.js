// var gulp = require('gulp');
// var coffee = require('coffee-script');

require('coffee-script/register');
require('./gulpfile.coffee');

// var pleeease = require('gulp-pleeease');
// var compass = require('gulp-compass');
// var imagemin = require('gulp-imagemin');
// var watch = require('gulp-watch');
// var browserSync = require('browser-sync');
// var bowerFiles = require('gulp-bower-files');
// var flatten = require('gulp-flatten');
// var notify = require('gulp-notify');
// var plumber = require('gulp-plumber');


// gulp.task('watch',['bsReload'], function() {
// 	gulp.watch('public/**/*.html',['change-html']);
// 	gulp.watch('public/**/*.php',['change-html']);
// 	gulp.watch('public/**/*.css',['change-html']);
// 	gulp.watch('_scss/**.scss',['compass']);
// 	gulp.watch('public/img/*.{png,jpg,gif}',['imagemin']);
// });


// //error notification settings for plumber
// var plumberErrorHandler = { errorHandler: notify.onError({
// 		message: "Error: <%= error.message %>"
// 	})
// };

// gulp.task('change-html',function(){
// 	return gulp.src('public/**/*')
// 		.pipe(browserSync.reload({stream: true, once: true}));
// });

// gulp.task('bower',function() {
// 	bowerFiles()
// 	// .pipe(gulp.flatten())
// 	.pipe(gulp.dest('public/js'));
// });

// gulp.task('compass',function(){
// 	return gulp.src('_scss/**.scss')
// 		.pipe(plumber(plumberErrorHandler))
// 		.pipe(compass({
// 			config_file: './config.rb',
// 			sass: '_scss',
// 			css: 'public/css',
// 			environment: 'dev',
// 			sourcemap: true
// 	}))
// 		.pipe(pleeease({
// 			fallbacks: {
// 				autoprefixer: ['last 4 versions', 'ios 7', 'ios 6', 'Android 2.3']
// 			},
// 			minifier: false
// 	}))
// 		.pipe(gulp.dest('public/css/'))
// 		// .pipe(notify({message:"reload"}))
// 		.pipe(browserSync.reload({stream: true, once: true}));
// });

// gulp.task('pls', function() {
// 	return gulp.src('./public/css/*.css')
// 		.pipe(pleeease({
// 			fallbacks: {
// 				autoprefixer: ['last 4 versions', 'ios 7', 'ios 6']
// 			},
// 			optimizers: {
// 				minifier: false
// 			}
// 	}))
// 		.pipe(gulp.dest('public/css/'))
// 		.pipe(browserSync.reload({stream: true, once: true}));
// });

// gulp.task('imagemin', function() {
// 	gulp.src('public/img/**/*.{png,jpg,gif}')
// 		.pipe(imagemin())
// 		.pipe(gulp.dest('public/img/'));
// });

// gulp.task('bsReload', function() {
// 	browserSync.init(null,{
// 		notify: true,
// 		browser: ["google chrome canary"],
// 		proxy: 'akiba.sivair'
// 		// proxy: 'akb.siva'
// 	})
// });

// // gulp.task('default', function() {
// // });