gulp = require 'gulp'
gutil = require 'gulp-util'

jade = require 'gulp-jade'

pleeease = require 'gulp-pleeease'
compass = require 'gulp-compass'
plumber = require 'gulp-plumber'

imagemin = require 'gulp-imagemin'

watch = require 'gulp-watch'
browserSync = require 'browser-sync'
flatten = require 'gulp-flatten'
uglify = require 'gulp-uglify'
notify = require 'gulp-notify'
Notifier = require 'node-notifier'
cond = require 'gulp-if'
gulpFilter = require 'gulp-filter'
mainBowerFiles = require 'main-bower-files'

notifier = new Notifier()

# isRelease = gutil.env.release?

gulp.task 'watch', ['bsReload'], ->
	gulp.watch 'jade/**/*.jade', ['jade']
	gulp.watch 'public/**/*.html', ['change-html']
	# gulp.watch 'public/**/*.php', ['change-html']
	gulp.watch 'public/**/css', ['change-html']
	gulp.watch '_scss/**/*.scss',['compass']
	gulp.watch 'public/img/*.{png,jpg,gif}', ['imagemin']


gulp.task 'change-html', ->
	gulp.src 'pub/**/*.html'
	.pipe browserSync.reload(stream: true, once: true)

gulp.task 'jade', ->
	gulp.src 'jade/**/*.jade'
	.pipe jade pretty: true
	.pipe gulp.dest('./public/')
	.pipe browserSync.reload(stream: true, once: true)


errorHandler = (error) ->
	notifier.notify
		message: error.message
		title: error.plugin
		sound: 'Glass'


gulp.task 'compass', ->
	gulp.src '_scss/**/*.scss'
	.pipe plumber
		errorHandler: errorHandler
	.pipe compass
		config_file :	'./config.rb'
		sass :			'_scss/'
		css:			'public/css'
		environment :	'dev',
		sourcemap :		true
	.pipe pleeease
		fallbacks:
			autoprefixer:	{'last 4 versions', 'ios 7', 'ios 6', 'Android 2.3'}
			minifier :		false
	.pipe gulp.dest 'public/css/'
	.pipe notify(message: 'reload')
	.pipe browserSync.reload(stream: true, once: true)

gulp.task 'imagemin', ->
	gulp.src 'public/img/**/*.{png,jpg,gif}'
	.pipe imagemin()
	.pipe gulp.dest 'public/img'

gulp.task 'bsReload', ->
	browserSync.init( null, {
		notify: true
		browser: "google chrome canary"
		# proxy: 'sivair'
		server:
			baseDir: './public/'
	})

gulp.task 'notify', ->
	notifier.notify
		title: 'test message'
		message: 'test'
		sound: 'Glass'

gulp.task 'bower', ->
	jsFilter = gulpFilter('**/*.js')
	cssFilter = gulpFilter('**/*.css')
	fontFilter = gulpFilter('**/*.{woff,eot,ttf,otf}')
	console.log(mainBowerFiles())
	gulp.src mainBowerFiles()
		.pipe jsFilter
		.pipe uglify({preserveComments:'some'})
		.pipe flatten()
		.pipe gulp.dest 'public/js'
		.pipe jsFilter.restore()
		.pipe cssFilter
		.pipe gulp.dest 'public/css'

