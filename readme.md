# 使い方

1. `/js/ga.js`にGoogle AnalyticsのIDを入力します。
2. 不要なJSを削除します。

# ajaxzip3.min.jsについて

## ajaxzip3.min.jsとは

郵便番号を入れると、都道府県や市区町村番地を自動入力するプログラムです。

フォームでは以下のように`AjaxZip3.zip2addr('zip','','pref','add1','add2');`という形で関数を呼びます。

+ 第1引数 郵便番号のname属性値
+ 第2引数 空（郵便番号を分ける場合にのみ、2つ目の郵便番号のテキストフィールドのname属性を挿入します。）
+ 第3引数 都道府県のname属性値
+ 第4引数 市区町村番地のname属性値
+ 第5引数 建物名のname属性値


以下は通常使われるフォームの入力例です。

```
<dl>
<dt>郵便番号</dt>
<dd><input type="text" name="zip" value="" onKeyUp="AjaxZip3.zip2addr('zip','','pref','add1','add2');"></dd>
<dt>都道府県</dt>
<dd><select name="pref">
<option value="">選択してください</option>
<option value="1" >北海道</option>
<option value="2" >青森県</option>
<option value="3" >岩手県</option>
（中略）
</select></dd>
<dt>市区町村番地</dt>
<dd><input type="text" name="add1" value=""></dd>
<dt>建物名</dt>
<dd><input type="text" name="add2" value=""></dd>
</dl>
```

## 参考

https://code.google.com/p/ajaxzip3/

